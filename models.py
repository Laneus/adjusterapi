from app import db


class Vlan(db.Model):
    __tablename__ = 'vlans'
    tag = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    name = db.Column(db.String(250))
    ports = db.relationship('PortVlan')

    def __init__(self, tag, name):
        self.tag = tag
        self.name = name

    def __repr__(self):
        return "Vlan (tag={}, name='{}')".format(self.tag, self.name)


class Host(db.Model):
    __tablename__ = 'hosts'
    ip = db.Column(db.String(250))
    name = db.Column(db.String(250), unique=True, nullable=False, primary_key=True)
    ports = db.relationship("Port", backref='host', cascade="save-update, merge, "
                                                            "delete, delete-orphan")
    os = db.Column(db.String(250))

    def __init__(self, ip, name, os):
        self.ip = ip
        self.name = name
        self.os = os

    def __repr__(self):
        return "Host (ip='{}', name='{}')".format(self.ip, self.name)

    def add_port(self, name, description):
        port = Port.query.get((name, self.name))
        if port is not None:
            raise ValueError('Record is already exists')
        port_new = Port(name=name, description=description, host_name=self.name)
        db.session.add(port_new)
        db.session.commit()
        return True


class Port(db.Model):
    __tablename__ = 'ports'
    name = db.Column(db.String(64), primary_key=True, nullable=False, autoincrement=False)
    description = db.Column(db.String(255))
    host_name = db.Column(db.String(), db.ForeignKey('hosts.name'), nullable=False, primary_key=True)
    vlans = db.relationship('PortVlan')

    def __init__(self, name, description, host_name):
        self.name = name
        self.host_name = host_name
        self.description = description

    def __repr__(self):
        return "Port (name='{}', description='{}', host_name='{}')".format(self.name, self.description, self.host_name)

    def add_vlan(self, vlan, tagged):
        pv = PortVlan.query.filter(PortVlan.vlan_tag == vlan.tag). \
            filter(PortVlan.port_name == self.name).\
            filter(PortVlan.host_name == self.host_name).first()
        if pv is not None:
            raise ValueError('Record is already exists')
        pv = PortVlan(vlan=vlan, port=self, tagged=tagged)
        db.session.add(pv)
        db.session.commit()
        return True

    def del_vlan(self, vlan):
        pv = PortVlan.query.filter(PortVlan.vlan_tag == vlan.tag). \
            filter(PortVlan.port_name == self.name).\
            filter(PortVlan.host_name == self.host_name).first()
        if pv is None:
            raise ValueError('Record is not exists')
        db.session.delete(pv)
        db.session.commit()
        return True


class PortVlan(db.Model):
    __tablename__ = 'port_vlans'

    __table_args__ = (
        db.ForeignKeyConstraint(
            ['port_name', 'host_name'],
            ['ports.name', 'ports.host_name'],
        ),
    )

    port_name = db.Column(db.String(), primary_key=True)
    host_name = db.Column(db.String(), primary_key=True)
    port = db.relationship('Port')

    vlan_tag = db.Column(db.Integer, db.ForeignKey('vlans.tag'), primary_key=True)
    vlan = db.relationship('Vlan')

    tagged = db.Column(db.Boolean)

    def __init__(self, vlan, port, tagged):
        self.vlan = vlan
        self.tagged = tagged
        self.port = port

    def __repr__(self):
        return "PortVlan (port_name='{}', host_name='{}', vlan_tag={}, tagged={})".format(self.port_name,
                                                                                          self.host_name,
                                                                                          self.vlan_tag,
                                                                                          self.tagged)
