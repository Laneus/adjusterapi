# git pull
sudo docker build -t adjuster .
sudo docker stop adjuster
sudo docker rm adjuster
sudo docker run -d --name adjuster --restart always -v /etc/localtime:/etc/localtime:ro -v adjuster-db:/db -p 127.0.0.1:8000:8000 adjuster
