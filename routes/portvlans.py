from app import app
from models import Vlan, Port, PortVlan
from schemas import VlanSchema, PortSchema, PortVlanSchema
from flask import request, abort
from routes.functions import get_obj, post_obj, del_obj


@app.route('/hosts/<string:host_name>/ports/<string:name>/vlans', methods=['GET'])
def get_vlans_of_port(host_name, name):
    name = name.replace('_', '/')
    filters = dict()
    filters['port'] = Port.query.get((name, host_name))
    return get_obj(PortVlan, PortVlanSchema, filters=filters, exclude=['port'])


@app.route('/hosts/<string:host_name>/ports/<string:name>/vlans/<int:tag>', methods=['GET'])
def get_one_vlan_on_port(host_name, name, tag):
    name = name.replace('_', '/')
    port = Port.query.get((name, host_name))
    vlan = Vlan.query.get(tag)
    return get_obj(PortVlan, PortVlanSchema, key=(port.name, port.host_name, vlan.tag))


@app.route('/hosts/<string:host_name>/ports/<string:name>/vlans', methods=['POST'])
def add_vlan_to_port(host_name, name):
    name = name.replace('_', '/')

    vlan = Vlan.query.get(request.json.get('tag'))
    if vlan:
        request.json['vlan'] = VlanSchema().dump(vlan).data
    else:
        return abort(404, 'Vlan not exist')

    port = Port.query.get((name, host_name))
    if port:
        request.json['port'] = PortSchema().dump(port).data
    else:
        return abort(404, 'Port not exist')

    request.json['tagged'] = request.json.get('tagged', False)
    return post_obj(PortVlan, PortVlanSchema)


@app.route('/hosts/<string:host_name>/ports/<string:name>/vlans/<int:tag>', methods=['DELETE'])
def del_vlan_to_port(host_name, name, tag):
    name = name.replace('_', '/')
    vlan = Vlan.query.get(tag)
    if not vlan:
        return abort(404, 'Vlan not exist')

    port = Port.query.get((name, host_name))
    if not port:
        return abort(404, 'Port not exist')

    return del_obj(PortVlan, key=(port.name, port.host_name, vlan.tag))