from app import app
from flask import jsonify, request


def _get_additional(e):
    try:
        additional = e.additional
    except AttributeError:
        additional = None
    return additional


def _get_error_dict(e, text, status):
    result = dict(message=text + ": " + request.url,
                  status=status)
    additional = _get_additional(e)
    if additional:
        result['additional'] = additional
    return result


@app.errorhandler(404)
def page_not_found(e):
    return jsonify(_get_error_dict(e, 'Not found', 404)), 404


@app.errorhandler(400)
def malformed(e):
    return jsonify(_get_error_dict(e, 'Bad Request', 400)), 400


@app.errorhandler(405)
def malformed(e):
    return jsonify(_get_error_dict(e, 'Method not allowed', 405)), 405


@app.errorhandler(409)
def malformed(e):
    return jsonify(_get_error_dict(e, 'Malformed request', 409)), 409


@app.errorhandler(500)
def internal_error(e):
    return jsonify(_get_error_dict(e, 'Internal error', 500)), 500
