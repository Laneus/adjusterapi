from models import Vlan
from schemas import VlanSchema
from app import app
from routes.functions import get_obj, del_obj, post_obj, put_obj


@app.route('/vlans', methods=['GET'])
def all_vlans():
    return get_obj(Vlan, VlanSchema, exclude=['ports'])


@app.route('/vlans/<int:tag>', methods=['GET'])
def single_vlan(tag):
    return get_obj(Vlan, VlanSchema, key=tag)


@app.route('/vlans', methods=['POST'])
def create_vlan():
    return post_obj(Vlan, VlanSchema)


@app.route('/vlans/<int:tag>', methods=['PUT'])
def update_vlan(tag):
    return put_obj(Vlan, VlanSchema, key=tag)


@app.route('/vlans/<int:tag>', methods=['DELETE'])
def delete_vlan(tag):
    return del_obj(Vlan, key=tag)
