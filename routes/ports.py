from models import Port
from flask import request
from schemas import PortSchema
from app import app
from routes.functions import get_obj, del_obj, post_obj, put_obj


@app.route('/hosts/<string:host_name>/ports', methods=['GET'])
@app.route('/ports', methods=['GET'])
def all_ports(host_name=None):
    filters = dict()
    exclude = list()

    if host_name is not None:
        filters['host_name'] = host_name
        exclude.append('host')

    return get_obj(Port, PortSchema, filters=filters, exclude=exclude)


@app.route('/hosts/<string:host_name>/ports/<string:name>', methods=['GET'])
def single_port(host_name, name):
    name = name.replace('_', '/')
    return get_obj(Port, PortSchema, key=(name, host_name))


@app.route('/hosts/<string:host_name>/ports/<string:name>', methods=['DELETE'])
def delete_port(host_name, name):
    name = name.replace('_', '/')
    return del_obj(Port, key=(name, host_name))


@app.route('/hosts/<string:host_name>/ports', methods=['POST'])
@app.route('/ports', methods=['POST'])
def create_ports(host_name=None):
    if host_name is not None:
        request.json['host_name'] = host_name
    return post_obj(Port, PortSchema)


@app.route('/hosts/<string:host_name>/ports/<string:name>', methods=['PUT'])
def update_port(host_name, name):
    name = name.replace('_', '/')
    return put_obj(Port, PortSchema, key=(name, host_name))