from models import Host
from schemas import HostSchema
from app import app
from routes.functions import get_obj, del_obj, post_obj, put_obj


@app.route('/hosts', methods=['GET'])
def all_hosts():
    return get_obj(Host, HostSchema, exclude=['ports'])


@app.route('/hosts/<string:name>', methods=['GET'])
def single_host(name):
    return get_obj(Host, HostSchema, key=name)


@app.route('/hosts', methods=['POST'])
def create_host():
    return post_obj(Host, HostSchema)


@app.route('/hosts/<string:name>', methods=['PUT'])
def update_host(name):
    return put_obj(Host, HostSchema, key=name)


@app.route('/hosts/<string:name>', methods=['DELETE'])
def delete_host(name):
    return del_obj(Host, key=name)
