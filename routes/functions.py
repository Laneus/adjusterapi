from flask import request, jsonify, abort
from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import FlushError
from sqlalchemy.inspection import inspect
from marshmallow.fields import String as mm_String
from marshmallow.fields import Integer as mm_Integer
from app import db


def get_obj(cls, schema, key=None, filters=None, exclude=None):
    if filters is None:
        filters = []
    if exclude is None:
        exclude = []

    query = cls.query
    query = _search(query, cls, schema)

    for _filter in filters:
        query = query.filter(getattr(cls, _filter) == filters[_filter])

    if key is None:
        limit = request.args.get('limit', 20)
        offset = request.args.get('offset', 0)
        count = query.count()
        query = query.limit(limit).offset(offset)
        obj = query.all()
        many = True
    else:
        obj = query.get_or_404(key)
        limit = 0
        offset = 0
        count = 1
        many = False

    fields = _get_fields_arg()
    data, errors = schema(exclude=exclude, only=fields).dump(obj, many=many)
    return jsonify(dict(data=data, errors=errors, count=count, limit=limit, offset=offset))


def del_obj(cls, key):
    obj = cls.query.get_or_404(key)
    db.session.delete(obj)
    db.session.commit()
    return jsonify(dict(message='Deleted')), 200


def post_obj(cls, schema):
    json = request.get_json()
    if not json:
        return abort(400)
    obj, errors = schema().load(json)
    if errors:
        return abort(400, errors)
    if _is_obj_exist(cls, obj):
        return abort(400, cls.__name__ + ': Record already exist')
    if obj:
        try:
            db.session.add(obj)
            db.session.commit()
            data, errors = schema().dump(obj)
            return jsonify(dict(data=data, errors=errors, message='Added')), 201
        except (IntegrityError, FlushError):
            db.session.rollback()
            return abort(400, cls.__name__ + ': Record already exist')
    else:
        return abort(500)


def put_obj(cls, schema, key):
    json = request.get_json()
    if not json:
        return abort(400)
    obj, errors = schema().load(json, instance=cls.query.get_or_404(key), partial=True)
    if errors:
        return abort(400, errors)
    if obj:
        db.session.add(obj)
        db.session.commit()
    data, errors = schema().dump(obj)
    return jsonify(dict(data=data, errors=errors, message='Updated')), 200


def _is_obj_exist(cls, obj):
    primary_keys = [x.name for x in inspect(cls).primary_key]
    query = cls.query
    for pk in primary_keys:
        query = query.filter(getattr(cls, pk) == getattr(obj, pk))
    return bool(query.first())


def _search(query, cls, schema):
    q = request.args.get('q')
    cls_fields = schema().fields
    if q:
        search = '%{}%'.format(str(q))
        search_fields = [x for x in cls_fields
                         if isinstance(cls_fields[x], mm_String)
                         or isinstance(cls_fields[x], mm_Integer)]
        query = query.filter(or_((getattr(cls, x).like(search) for x in search_fields)))
    else:
        for field in cls_fields:
            f = request.args.get(field)
            if f:
                query = query.filter(getattr(cls, field).like('%{}%'.format(f)))
    return query


def _get_fields_arg():
    fields = request.args.get('fields')
    if fields is not None:
        fields = tuple(x for x in fields.split(','))
    return fields
