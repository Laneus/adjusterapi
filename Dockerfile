FROM python:3-alpine
RUN mkdir /db
COPY . /app
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install gunicorn
RUN pip install -r requirements.txt
ENTRYPOINT ["gunicorn", "-w", "4", "-b","0.0.0.0:8000", "--threads", "10", "app:app"]
