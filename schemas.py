from app import ma
from models import Vlan, Port, Host, PortVlan
from marshmallow import fields, ValidationError, pre_load, post_load
import re


def validate_ip(ip):
    address = re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", ip)
    if not address:
        raise ValidationError('Defined IP is not valid')


def validate_vlan(tag):
    if not 0 < tag <= 4096:
        raise ValidationError('VLAN ID is out of possible range')


class HostSchema(ma.ModelSchema):
    class Meta:
        model = Host
    ip = fields.Str(validate=validate_ip, required=True)
    name = fields.Str(required=True, allow_none=False)
    os = fields.Str()
    ports = ma.Nested('PortSchema', exclude=('host',), many=True, only_dump=True)
    # _link = ma.URLFor('single_host', ip='<ip>')


class PortSchema(ma.ModelSchema):
    class Meta:
        model = Port
    name = fields.Str(required=True)
    description = fields.Str(allow_none=False)
    host_name = fields.Str(required=True)
    host = ma.Nested('HostSchema', exclude=('ports',), only_dump=True)
    vlans = ma.Nested('PortVlanSchema', many=True, exclude=('port',), only_dump=True)
    # _link = ma.URLFor('singleport', name='<name>', host_ip='<host_ip>', only_dump=True)


class VlanSchema(ma.ModelSchema):
    class Meta:
        model = Vlan
    tag = fields.Int(validate=validate_vlan, required=True)
    name = fields.Str(required=True, allow_none=False)
    ports = ma.Nested('PortVlanSchema', many=True, exclude=('vlan',), only_dump=True)
    # _link = ma.URLFor('singlevlan', tag='<tag>', only_dump=True)


class PortVlanSchema(ma.ModelSchema):
    class Meta:
        model = PortVlan
    port = ma.Nested('PortSchema', exclude=('vlans',))
    vlan = ma.Nested('VlanSchema', exclude=('ports',))
    tagged = fields.Boolean()
